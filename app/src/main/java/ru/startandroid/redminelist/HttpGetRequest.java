package ru.startandroid.redminelist;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user on 14.09.2018.
 */

public class HttpGetRequest extends AsyncTask<URL, Void, String> {

    private ProjectActivity projectActivity;



    public HttpGetRequest() {
    }

    public void setProjectActivity(ProjectActivity newProjectActivity) {
        this.projectActivity = newProjectActivity;
    }


    @Override
    protected String doInBackground(URL... urls) {


        try {

            //String url = "https://rm.stagingmonster.com/projects.json?key=d2bc591aef705a14dfc8a30c70fd060266f270f0";
            URL obj = new URL(ProjectActivity.projUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + ProjectActivity.projUrl);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
            return response.toString();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        projectActivity.MyPostExecute(result);
    }

}
