package ru.startandroid.redminelist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static ru.startandroid.redminelist.ProjectActivity.projUrl;


public class IssueActivity extends Activity implements IssueRecyclerViewAdapter.ItemClickListener{
    List<Issue> issueLst = new ArrayList<>();
    IssueRecyclerViewAdapter adapter;
    //public final static String issueUrl = "https://rm.stagingmonster.com/projects/".concat(String.valueOf(mProject)).concat("/issues.json?key=d2bc591aef705a14dfc8a30c70fd060266f270f0");
    String issueUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issuerecyclerview);


        IssueAsynkTask issueAsynkTask = new IssueAsynkTask(this);
//        issueAsynkTask.setIssueActivity(this);
        issueAsynkTask.execute();

        Project mProject = (Project) getIntent().getSerializableExtra(projUrl);
        issueUrl = "https://rm.stagingmonster.com/projects/".concat(String.valueOf(mProject.getId()).concat("/issues.json?key=d2bc591aef705a14dfc8a30c70fd060266f270f0"));
Log.d("myLog", "URL  " + issueUrl);
    }

    public void onItemClick(View view, int position) {


        Intent intent = new Intent(this, IssueActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(issueUrl, adapter.getItem(position));
        intent.putExtras(mBundle);

        startActivity(intent);
    }

    public void IssuePostExecute(String result) {

        String res = result;
        JSONArray issues = null;


        try {
            issues = new JSONObject(res).getJSONArray("issues");
Log.d("MyLogs", "Issues      " + issues);
            for (int i = 0; i < issues.length(); i++) {


                JSONObject o = issues.getJSONObject(i);

                Issue iss = new Issue();
                iss.setId(o.getInt("id"));
                iss.setSubject(o.getString("subject"));
                iss.setAssigned(o.getString("assigned_to"));
                iss.setUpdated(o.getString("updated_on"));

                issueLst.add(iss);

            }
            Log.d("MyLogs", "IssueLst" + String.valueOf(issueLst));

        } catch (JSONException e) {
            //e.printStackTrace();
        }

        RecyclerView rv = (RecyclerView) findViewById(R.id.issue_recycler_view);

        Context context = IssueActivity.this;
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);

        adapter = new IssueRecyclerViewAdapter(IssueActivity.this, issueLst);
        adapter.setClickListener(IssueActivity.this);
        rv.setAdapter(adapter);

        Log.d("MyLogs", "   SetAdapter        ");

    }

}
