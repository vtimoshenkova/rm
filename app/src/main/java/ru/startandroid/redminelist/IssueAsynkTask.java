package ru.startandroid.redminelist;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user on 20.09.2018.
 */

public class IssueAsynkTask extends AsyncTask<URL, Void, String> {

    private IssueActivity issueActivity;


    public IssueAsynkTask(IssueActivity issueActivity) {

        this.issueActivity = issueActivity;
    }
//        public void setIssueActivity(IssueActivity newIssueActivity) {
//        this.issueActivity = newIssueActivity;
//    }

    @Override
    protected String doInBackground(URL... urls) {
        String result = null;
        String inputLine;

        try {

            URL obj = new URL(issueActivity.issueUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.connect();
            InputStreamReader streamReader = new InputStreamReader(con.getInputStream());

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + issueActivity.issueUrl);
            System.out.println("Response Code : " + responseCode);
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            reader.close();
            streamReader.close();

            result = stringBuilder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("MyLogs", "Result doInBack    " + result);
        return result;
    }


    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        issueActivity.IssuePostExecute(result);

    }
}
