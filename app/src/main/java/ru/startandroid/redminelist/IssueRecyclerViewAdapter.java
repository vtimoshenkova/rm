package ru.startandroid.redminelist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static ru.startandroid.redminelist.R.layout.issue;

/**
 * Created by user on 20.09.2018.
 */

public class IssueRecyclerViewAdapter extends RecyclerView.Adapter<IssueRecyclerViewAdapter.ViewHolder> {

    private List<Issue> issueData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;



    IssueRecyclerViewAdapter(Context context, List<Issue> data) {

        Log.d("MyLogs", "Data size         " + String.valueOf(data.size()));

        this.mInflater = LayoutInflater.from(context);
        this.issueData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(issue, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Issue issue = issueData.get(position);
        holder.issueSubject.setText(issue.getSubject());
        holder.issueId.setText(String.valueOf(issue.getId()));


        //Log.d("myLogs", position + " " + project.getName());
    }

    @Override
    public int getItemCount() {
        return issueData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView issueId;
        TextView issueSubject;

        ViewHolder(View itemView) {
            super(itemView);
            issueSubject = (TextView) itemView.findViewById(R.id.issueSubject);
            issueId = (TextView) itemView.findViewById(R.id.issueId);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    private void startActivity(Intent intent) {
    }

    Issue getItem(int id) {
        return issueData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;


    }

    public IssueRecyclerViewAdapter(List<Issue> issueData, ItemClickListener mClickListener) {
        this.issueData = issueData;
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}
