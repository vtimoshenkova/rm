package ru.startandroid.redminelist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class ProjectActivity extends Activity implements ProjectRecyclerViewAdapter.ItemClickListener {

    List<Project> projLst = new ArrayList<>();
    ProjectRecyclerViewAdapter adapter;
    EditText et;
    public final static String projUrl = "https://rm.stagingmonster.com/projects.json?key=d2bc591aef705a14dfc8a30c70fd060266f270f0";




    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview);
        et = (EditText) findViewById(R.id.et);


        HttpGetRequest httpGetRequest = new HttpGetRequest();
        httpGetRequest.setProjectActivity(this);
        httpGetRequest.execute();

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

    }

    private void filter(String text) {
        List<Project> foundProjectList = new ArrayList<>();
        for(int i = 0; i < projLst.size(); i++){
           Project project = projLst.get(i);
            if (project.getName().toLowerCase().contains(text.toLowerCase())) {
                foundProjectList.add(project);
            }
        }
        adapter.filterList(foundProjectList);
    }



    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(this, IssueActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(projUrl, adapter.getItem(position));
        intent.putExtras(mBundle);

        startActivity(intent);
    }

    public void MyPostExecute(String result) {

        String res = result;
        JSONArray projects = null;

        try {
            projects = new JSONObject(res).getJSONArray("projects");

            for (int i = 0; i < projects.length(); i++) {
                Log.d("myLogs", "i" + i);

                JSONObject o = projects.getJSONObject(i);

                Project proj = new Project();
                proj.setId(o.getInt("id"));
                proj.setName(o.getString("name"));
                proj.setCreated(o.getString("created_on"));
                proj.setDescription(o.getString("description"));


                projLst.add(proj);
            }
            Log.d("myLogs", "projLst  " + String.valueOf(projLst));

        } catch (JSONException e) {
           // e.printStackTrace();
        }


        RecyclerView rv = (RecyclerView) findViewById(R.id.my_recycler_view);

        Context context = ProjectActivity.this;
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);

        adapter = new ProjectRecyclerViewAdapter(ProjectActivity.this, projLst);
        adapter.setClickListener(ProjectActivity.this);
        rv.setAdapter(adapter);

    }

}
