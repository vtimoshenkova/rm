package ru.startandroid.redminelist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ProjectRecyclerViewAdapter extends RecyclerView.Adapter<ProjectRecyclerViewAdapter.ViewHolder> {

    private List<Project> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    ProjectRecyclerViewAdapter(Context context, List<Project> data) {

        Log.d("myLogs", String.valueOf(data.size()));

        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Project project = mData.get(position);
        holder.myProjectName.setText(project.getName());
        holder.myIDProj.setText(String.valueOf(project.getId()));


        Log.d("myLogs", position + " " + project.getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView myProjectName;
        TextView myIDProj;

        ViewHolder(View itemView) {
            super(itemView);
            myProjectName = (TextView) itemView.findViewById(R.id.tvName);
            myIDProj = (TextView) itemView.findViewById(R.id.tvId);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

    private void startActivity(Intent intent) {
    }

    Project getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;


    }

    public ProjectRecyclerViewAdapter(List<Project> mData, ItemClickListener mClickListener) {
        this.mData = mData;
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void filterList(List<Project> foundProjectListForRV) {
        this.mData = foundProjectListForRV;
        notifyDataSetChanged();
    }
}